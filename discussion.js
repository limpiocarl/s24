// db.inventory.insertMany([
//   {
//     name: "Arduino",
//     price: 80000,
//     qty: 25,
//   },
//   {
//     name: "Raspberry Pi",
//     price: 70000,
//     qty: 15,
//   },
//   {
//     name: "WiFi Module",
//     price: 60000,
//     qty: 10,
//   },
// ]);

db.inventory.insertMany([
  {
    name: "Captain America Shield",
    price: 50000,
    qty: 17,
    company: "Hydra and Co",
  },
  {
    name: "Mjolnir",
    price: 75000,
    qty: 24,
    company: "Asgard Production",
  },
  {
    name: "Iron Man Suit",
    price: 25400,
    qty: 25,
    company: "Stark Industries",
  },
  {
    name: "Eye of Agamotto",
    price: 28000,
    qty: 51,
    company: "Sanctum Company",
  },
  {
    name: "Iron Spider Suit",
    price: 30000,
    qty: 24,
    company: "Stark Industries",
  },
]);

// Query Operators

// syntax $gt & $gte - greater than, greater than equal

db.collections.find({ field: { $gt: value } });
db.collections.find({ field: { $gte: value } });

db.inventory.find({ price: { $gte: 75000 } });

// $lt & $lte - less than, less than equal

db.inventory.find({ qty: { $lt: 20 } });

// $ne - not equal

db.inventory.find({ qty: { $ne: 10 } });

// $in - allows us to find documents that satisfy either of the specified values.

db.inventory.find({ price: { $in: [25400, 30000] } });

db.inventory.find({ price: { $gte: 50000 } });

db.inventory.find({ qty: { $in: [24, 16] } });

// Update and Delete
// 2 arguments namely: query criteria, update

db.inventory.updateMany({ qty: { $lte: 24 } }, { $set: { isActive: true } });

db.inventory.updateMany({ price: { $lt: 28000 } }, { $set: { qty: 17 } });

// Logical Operators
// $and
// syntax:
db.collections.find({ $and: [{ criteria1 }, { criteria2 }] });
// $and - allows us to return document/s that satisfies all given conditions.

db.inventory.find({ $and: [{ price: { $gte: 50000 } }, { qty: 17 }] });

// $or
// syntax:
db.collections.find({ or: [{ criteria1 }, { criteria2 }] });

db.inventory.find({ $or: [{ qty: { $lt: 24 } }, { isActive: false }] });

// mini-activity
db.inventory.updateMany(
  {
    $or: [{ qty: { $lte: 24 } }, { price: { $lte: 30000 } }],
  },
  { $set: { isActive: true } }
);

// Evaluation Query Operator
// $regex
// syntax

// case sensitive query
{
  field: {
    $regex: /pattern/;
  }
}

{
  field: {
    $regex: /pattern/;
    $options: "$optionValue";
  }
}

db.inventory.find({ name: { $regex: "S" } });

db.inventory.find({ name: { $regex: "A", $options: "$i" } });

// pattern
db.inventory.find({ name: { $regex: "A-M" } });

// mini-activiy
db.inventory.find({
  $and: [{ name: { $regex: "i", $options: "i" } }, { price: { $gt: 70000 } }],
});

// Field Projection
// allows us to hide or show properties of a return document/s after a query. When dealing with complex data structures, there might be instances when fields are not useful for the query we are trying to accomplish.

// inclusion and exclusion
// syntax:
db.collections.find({ criteria }, { field: 1 });
// field 1 - allows us to include/add specific fields only when retrieving documents. The value provide is 1 to denote that the field is being included.

// syntax:
db.collections.find({ criteria }, { field: 0 });
// field 0 - allows us to exclude the specific field

db.inventory.find({}, { name: 1 });

db.inventory.find({}, { qty: 0 });

db.inventory.find(
  { company: { $regex: "A" } },
  { name: 1, company: 1, _id: 0 }
);

// mini-activity
db.inventory.find(
  { $and: [{ name: { $regex: "A" } }, { price: { $lte: 30000 } }] },
  { name: 1, price: 1, _id: 0 }
);
